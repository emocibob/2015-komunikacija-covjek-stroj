% Autor:
%   Edvin Mocibob
% 
% Opis:
%   Vjezba 1 na MudRi.
addpath('voicebox');

% govornik s1 i s2.
% izlistaj direktorije if i mm
files_s1 = dir('if/*.wav');
files_s2 = dir('mm/*.wav');

% broj datoteka u direktorijima
num_files = size(files_s1, 1);
data_s1 = [];
data_s2 = [];
for fn = 1:num_files
        % return sampled data (s1, s2) and a sample rate for that data (fs)
        [s1, fs] = audioread(fullfile('if', files_s1(fn,:).name));
        [s2, fs] = audioread(fullfile('mm', files_s2(fn,:).name));
        % izlucuje znacajku f0
        data_s1 = [data_s1; fxrapt(s1, fs)];
        data_s2 = [data_s2; fxrapt(s2, fs)];
end

% figure(h)
% If h is not the handle to an existing figure, but is an integer,
% figure(h) creates a figure and assigns it the handle h
figure(1)
% subplot(m,n,p) divides the current figure into an m-by-n grid
% and creates an axes in the grid position specified by p
h1 = subplot(2,1,1);
% hist(data,nbins) sorts data into the number of bins specified by the scalar nbins
hist(data_s1,20);
% v = axis returns a row vector containing scaling factors for the x-, y-, and z-axis.
% v has four or six components depending on whether the current axes is 2-D or 3-D, respectively.
% The returned values are the current axes XLim, Ylim, and ZLim properties.
ax1 = axis();
h2 = subplot(2,1,2);
hist(data_s2,20);
ax2 = axis();
limits = [min(ax1(1), ax2(1)), max(ax1(2), ax2(2))];
set(h1, 'xlim', limits)
set(h2, 'xlim', limits)

% zadatak 1

% n.b.: koriste se opet ista imena varijabli
data_s1 = [];
data_s2 = [];
for fn = 1:num_files
        [s1, fs] = audioread(fullfile('if', files_s1(fn,:).name));
        [s2, fs] = audioread(fullfile('mm', files_s2(fn,:).name));
        data_s1 = [data_s1; melcepst(s1, fs)];
        data_s2 = [data_s2; melcepst(s2, fs)];
end

figure(2)
h1 = subplot(2,1,1);
hist(data_s1(:, 1:5),20);
ax1 = axis();
h2 = subplot(2,1,2);
hist(data_s2(:, 1:5),20);
ax2 = axis();
limits = [min(ax1(1), ax2(1)), max(ax1(2), ax2(2))];
set(h1, 'xlim', limits)
set(h2, 'xlim', limits)

%%%%%%%%
%%%%%%%% knn - melcepst podaci proslog zadatka
%%%%%%%%

% jedna matrica sa podacima s1 i s2
data_prvi = [data_s1; data_s2];
% labele za gornju matricu
data_prvi_labels = [repmat({'if'}, length(data_s1), 1); repmat({'mm'}, length(data_s2), 1)];

% konstantni random seed
rng(999);
% izmjesaj podatke
rnd_idx = randperm(length(data_prvi));
data_prvi = data_prvi(rnd_idx, :);
data_prvi_labels = data_prvi_labels(rnd_idx);

% trening prvih 90% podatka, klasifikcaija zadnjih 10% podataka
data_granica = round(length(data_prvi) * 0.9);
data_train = data_prvi(1:data_granica, :);
data_train_labels = data_prvi_labels(1:data_granica);
data_klas = data_prvi(data_granica+1:end, :);

% knn klasifikacija
% Class = knnclassify(Sample, Training, Group, k, distance)
klas_prvi = knnclassify(data_klas, data_train, data_train_labels, 5, 'euclidean');

% nadji tocnost klasificiranja
tocnih = cellfun(@strcmp, klas_prvi, data_prvi_labels(data_granica+1:end));
klas_tocnost = (sum(tocnih) / length(tocnih)) * 100;
fprintf('Tocnost 1. klasifikacije: %d / %d = %.2f%%\n', sum(tocnih), length(tocnih), klas_tocnost);