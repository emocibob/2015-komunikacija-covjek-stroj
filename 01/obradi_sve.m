% govornik s1 i s2.
files_s1 = dir('if/*.wav');
files_s2 = dir('mm/*.wav');

num_files = size(files_s1, 1);
data_s1 = [];
data_s2 = [];
for fn = 1:num_files
        [s1, fs] = audioread(fullfile('if', files_s1(fn,:).name));
        [s2, fs] = audioread(fullfile('mm', files_s2(fn,:).name));
        data_s1 = [data_s1; fxrapt(s1, fs)];
        data_s2 = [data_s2; fxrapt(s2, fs)];
end

figure(1)
h1 = subplot(2,1,1);
hist(data_s1,20);
ax1 = axis();
h2 = subplot(2,1,2);
hist(data_s2,20);
ax2 = axis();
limits = [min(ax1(1), ax2(1)), max(ax1(2), ax2(2))];
set(h1, 'xlim', limits)
set(h2, 'xlim', limits)
