% Autor:
%   Edvin Mocibob
% 
% Opis:
%   Testiranje nekih metoda klasifikacije u MATLAB-u.
%   Klasificiraju se snimke govora iz 'Baza ODGOS' (MudRi).
%
% NB:
%   Paziti gdje su folderi za addpath i slicno.
addpath('../02/voicebox');
addpath('../02/odogs zadaca');

% pripremi podatke

% sve mape imaju isti broj fileova
files_g1 = dir('../02/odogs zadaca/Ema_Matijevic/*.wav');
files_g2 = dir('../02/odogs zadaca/Kristina Ban/*.wav');
files_g3 = dir('../02/odogs zadaca/Dajana_Hajdu_PR2/*.wav');
files_g4 = dir('../02/odogs zadaca/IvanIvakic-snimke-govora/*.wav');

num_files2 = size(files_g1, 1);
num_files2 = round(num_files2/4);

data_g1 = [];
data_g2 = [];
data_g3 = [];
data_g4 = [];
for fn = 1:num_files2
        [g1, fs] = audioread(fullfile('../02/odogs zadaca/Ema_Matijevic', files_g1(fn, :).name));
        [g2, fs] = audioread(fullfile('../02/odogs zadaca/Kristina Ban', files_g2(fn, :).name));
        [g3, fs] = audioread(fullfile('../02/odogs zadaca/Dajana_Hajdu_PR2', files_g3(fn, :).name));
        [g4, fs] = audioread(fullfile('../02/odogs zadaca/IvanIvakic-snimke-govora', files_g4(fn, :).name));
        data_g1 = [data_g1; melcepst(g1, fs)];
        data_g2 = [data_g2; melcepst(g2, fs)];
        data_g3 = [data_g3; melcepst(g3, fs)];
        data_g4 = [data_g4; melcepst(g4, fs)];
end

data_drugi = [data_g1; data_g2; data_g3; data_g4];
data_drugi_labels = [repmat({'Ema_Matijevic'}, length(data_g1), 1);
                    repmat({'Kristina_Ban'}, length(data_g2), 1);
                    repmat({'Dajana_Hajdu_PR2'}, length(data_g3), 1);
                    repmat({'Ivan_Ivakic'}, length(data_g4), 1)];

rnd_idx2 = randperm(length(data_drugi));
data_drugi = data_drugi(rnd_idx2, :);
data_drugi_labels = data_drugi_labels(rnd_idx2);

data2_granica = round(length(data_drugi) * 0.9);
data2_train = data_drugi(1:data2_granica, :);
data2_train_labels = data_drugi_labels(1:data2_granica);
data2_klas = data_drugi(data2_granica+1:end, :);

%%%%%%%%
%%%%%%%% knn
%%%%%%%%

time_start = cputime;
klas_drugi = knnclassify(data2_klas, data2_train, data2_train_labels, 3, 'euclidean');
time_end = cputime;

tocnih2 = cellfun(@strcmp, klas_drugi, data_drugi_labels(data2_granica+1:end));
klas2_tocnost = (sum(tocnih2) / length(tocnih2)) * 100;
[C, order] = confusionmat(data_drugi_labels(data2_granica+1:end), klas_drugi);
fprintf('Broj podataka za treniranje (knn): %d (od ukupno %d)\n', length(data2_train), length(data_drugi));
fprintf('Tocnost knn klasifikacije: %d / %d = %.2f%%\n', sum(tocnih2), length(tocnih2), klas2_tocnost);
fprintf('Vrijeme izvodjenja za knn: %f s.\n', (time_end-time_start))
fprintf('Confusion matrix za knn:\n');
disp(C)

%%%%%%%%
%%%%%%%% Neural Network
%%%%%%%%

% podaci
data_net = [data_g1; data_g2];
data_net = data_net';
t1 = [ones(1, length(data_g1)); zeros(1, length(data_g1))];
t2 = [zeros(1, length(data_g2)); ones(1, length(data_g2))];
t = [t1, t2];

hiddenLayerSize = 10;
net = patternnet(hiddenLayerSize);
net.divideFcn = 'dividerand'; % izmijesaj podatke
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 15/100;
net.divideParam.testRatio = 15/100;
[net, tr] = train(net, data_net, t);
fprintf('Neural Network\n');
fprintf('Br. uzoraka za trening: %d\n', length(tr.trainInd));
fprintf('Br. uzoraka za validaciju: %d\n', length(tr.valInd));
fprintf('Br. uzoraka za testiranje: %d\n', length(tr.testInd));

outputs = net(data_net);
errors = gsubtract(t, outputs);
performance = perform(net, t, outputs)
view(net)
figure, plotconfusion(t, outputs)