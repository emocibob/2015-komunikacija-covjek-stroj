% Autor:
%   Edvin Mocibob
% 
% Opis:
%   Testiranje nekih metoda klasifikacije u MATLAB-u.
%   Klasificiraju se snimke govora iz 'Baza ODGOS' (MudRi).
%
% NB:
%   Preimenovati mape iz raspakirane arhive da pasu imenima u programu.
addpath('voicebox');
addpath('odogs zadaca');

% pripremi podatke

% sve mape imaju isti broj fileova
files_g1 = dir('odogs zadaca/Ema_Matijevic/*.wav');
files_g2 = dir('odogs zadaca/Kristina Ban/*.wav');
files_g3 = dir('odogs zadaca/Dajana_Hajdu_PR2/*.wav');
files_g4 = dir('odogs zadaca/IvanIvakic-snimke-govora/*.wav');

num_files2 = size(files_g1, 1);
num_files2 = round(num_files2/4);

data_g1 = [];
data_g2 = [];
data_g3 = [];
data_g4 = [];
for fn = 1:num_files2
        [g1, fs] = audioread(fullfile('odogs zadaca/Ema_Matijevic', files_g1(fn, :).name));
        [g2, fs] = audioread(fullfile('odogs zadaca/Kristina Ban', files_g2(fn, :).name));
        [g3, fs] = audioread(fullfile('odogs zadaca/Dajana_Hajdu_PR2', files_g3(fn, :).name));
        [g4, fs] = audioread(fullfile('odogs zadaca/IvanIvakic-snimke-govora', files_g4(fn, :).name));
        data_g1 = [data_g1; melcepst(g1, fs)];
        data_g2 = [data_g2; melcepst(g2, fs)];
        data_g3 = [data_g3; melcepst(g3, fs)];
        data_g4 = [data_g4; melcepst(g4, fs)];
end

data_drugi = [data_g1; data_g2; data_g3; data_g4];
data_drugi_labels = [repmat({'Ema_Matijevic'}, length(data_g1), 1);
                    repmat({'Kristina_Ban'}, length(data_g2), 1);
                    repmat({'Dajana_Hajdu_PR2'}, length(data_g3), 1);
                    repmat({'Ivan_Ivakic'}, length(data_g4), 1)];

rnd_idx2 = randperm(length(data_drugi));
data_drugi = data_drugi(rnd_idx2, :);
data_drugi_labels = data_drugi_labels(rnd_idx2);

data2_granica = round(length(data_drugi) * 0.9);
data2_train = data_drugi(1:data2_granica, :);
data2_train_labels = data_drugi_labels(1:data2_granica);
data2_klas = data_drugi(data2_granica+1:end, :);

%%%%%%%%
%%%%%%%% knn
%%%%%%%%

time_start = cputime;
klas_drugi = knnclassify(data2_klas, data2_train, data2_train_labels, 3, 'euclidean');
time_end = cputime;

tocnih2 = cellfun(@strcmp, klas_drugi, data_drugi_labels(data2_granica+1:end));
klas2_tocnost = (sum(tocnih2) / length(tocnih2)) * 100;
fprintf('Broj podataka za treniranje (knn): %d (od ukupno %d)\n', length(data2_train), length(data_drugi));
fprintf('Tocnost knn klasifikacije: %d / %d = %.2f%%\n', sum(tocnih2), length(tocnih2), klas2_tocnost);
fprintf('Vrijeme izvodjenja za knn: %f s.\n', (time_end-time_start))

%%%%%%%%
%%%%%%%% NaiveBayes
%%%%%%%%

time_start = cputime;
nb = NaiveBayes.fit(data2_train, data2_train_labels);
klas3_drugi = predict(nb, data2_klas);
time_end = cputime;

tocnih3 = cellfun(@strcmp, klas3_drugi, data_drugi_labels(data2_granica+1:end));
klas3_tocnost = (sum(tocnih3) / length(tocnih3)) * 100;
fprintf('Broj podataka za treniranje (NaiveBayes): %d (od ukupno %d)\n', length(data2_train), length(data_drugi));
fprintf('Tocnost NaiveBayes klasifikacije: %d / %d = %.2f%%\n', sum(tocnih3), length(tocnih3), klas3_tocnost);
fprintf('Vrijeme izvodjenja za NaiveBayes: %f s.\n', (time_end-time_start))

%%%%%%%%
%%%%%%%% TreeBagger
%%%%%%%%

time_start = cputime;
B = TreeBagger(10, data2_train, data2_train_labels); % broj stabala sam stavio bezveze
Y = predict(B, data2_klas);
time_end = cputime;

tocnih4 = cellfun(@strcmp, Y, data_drugi_labels(data2_granica+1:end));
klas4_tocnost = (sum(tocnih4) / length(tocnih4)) * 100;
fprintf('Broj podataka za treniranje (TreeBagger): %d (od ukupno %d)\n', length(data2_train), length(data_drugi));
fprintf('Tocnost TreeBagger klasifikacije: %d / %d = %.2f%%\n', sum(tocnih4), length(tocnih4), klas4_tocnost);
fprintf('Vrijeme izvodjenja za TreeBagger: %f s.\n', (time_end-time_start))